<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index']);
Route::get('/shop', [\App\Http\Controllers\ShopController::class, 'index']);
Route::get('/shopcart', [\App\Http\Controllers\ShoppingCartController::class, 'index']);
Route::get('/checkout', [\App\Http\Controllers\CheckOutController::class, 'index']);

Route::get('/profile', [\App\Http\Controllers\ProfileController::class, 'index']);
Route::post('/regis', [\App\Http\Controllers\RegisterController::class, 'regis']);

Route::post('/profile/create', [\App\Http\Controllers\ProfileController::class, 'store']);

Route::get('/logOUT', [\App\Http\Controllers\RegisterController::class, 'logout']);
Auth::routes();

Route::group(['middleware' => ['auth', 'isAdmin'], 'prefix' => 'admin'], function () {
    Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class, 'index']);
    // Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
});


//route Category
Route::get('/categories', [\App\Http\Controllers\CategoryController::class, 'index']);
Route::get('/categories/create', [\App\Http\Controllers\CategoryController::class, 'create']);
Route::post('/categories/create', [\App\Http\Controllers\CategoryController::class, 'store']);
Route::post('/categories/delete/{id}', [\App\Http\Controllers\CategoryController::class, 'destroy']);

Route::get('/categories/edit/{id}', [\App\Http\Controllers\CategoryController::class, 'edit']);
Route::put('/categories/update/{id}', [\App\Http\Controllers\CategoryController::class, 'update']);

//route review
Route::get('/review', [\App\Http\Controllers\ReviewController::class, 'index']);
Route::get('/create/review', [\App\Http\Controllers\ReviewController::class, 'create']);
Route::post('/review/create', [\App\Http\Controllers\ReviewController::class, 'store']);

Route::get('/review/edit/{id}', [\App\Http\Controllers\ReviewController::class, 'edit']);
// Route::get('/review/update/{$id}', [\App\Http\Controllers\ReviewController::class, 'edit']);
Route::put('/review/update/{id}', [\App\Http\Controllers\ReviewController::class, 'update']);
Route::post('/review/delete/{id}', [\App\Http\Controllers\ReviewController::class, 'destroy']);

// Route::post('/review/delete/{$id}', [\App\Http\Controllers\ReviewController::class, 'destroy']);
// Route::resource('review', 'ReviewController');

//route product admin
Route::get('/product', [\App\Http\Controllers\ProductController::class, 'index']);
Route::get('/create', [\App\Http\Controllers\ProductController::class, 'create']);
Route::post('/product/create', [\App\Http\Controllers\ProductController::class, 'store']);
Route::get('/product/edit/{id}', [\App\Http\Controllers\ProductController::class, 'edit']);
Route::put('/product/update/{id}', [\App\Http\Controllers\ProductController::class, 'update']);
Route::post('/product/delete/{id}', [\App\Http\Controllers\ProductController::class, 'destroy']);


Route::get('LaporanP', [\App\Http\Controllers\LaporanPenjualanController::class, 'index']);


//show
