Ketentuan
Aplikasikan materi-materi yang telah dipelajari sebelumnya di antaranya:

Membuat ERD
Migrations
Model + Eloquent
Controller
Laravel Auth + Middleware
View (Blade)
CRUD (Create Read Update Delete)
Eloquent Relationships
Laravel + Library/Packages

Pilihan 1 : Tema Sendiri (Kecuali Blog & Portal Berita)
Requirement Minimum :

1   Menggunakan Laravel versi 8 atau versi 8 keatas
2   Minimum terdapat 3 fitur CRUD (Misalkan jika temanya e-commerce maka minimal ada CRUD produk, CRUD kategori, CRUD keranjang, dst.)
3   Template tampilan dapat memilih sendiri, pilihlah template yang cocok untuk tema project dan gratis.  link template gratis : https://themewagon.com/theme_tag/bootstrap-4-templates/ 
4   Harus menggunakan Authentication (Laravel Auth)
5   Terdapat fitur yang menggunakan Eloquent Relationship (One To One, One To Many, Many To Many)
6   Minimum memasangkan 3 Library/Package di luar Laravel Contohnya : sweet alert, laravel excel, laravel pdf, laravel file manager, Datatables, TinyMCE, dll.
7   Deploy ke Heroku atau share hosting